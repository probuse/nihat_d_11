# -*- coding: utf-8 -*-

from datetime import datetime, date, timedelta as td

from odoo import api, models, fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning


class ReportHrAttendance(models.AbstractModel):
    _name = 'report.hr_attendance_report.hr_attendance_report'
    
    @api.model
#     def render_html(self, docids, data=None):
    def get_report_values(self, docids, data=None):
        attendance_ids = False
        if data['report_for'] == 'detail':
            attendance_ids = self.env['hr.attendance'].search([
                        ('employee_id', '=', data['employee_id'][0]),
                        ('check_in', '>=', data['start_date']),
                        ('check_in', '<=', data['end_date'])],
                        order="check_in asc"
                    )
        if data['report_for'] == 'summary':
            employees = self.env['hr.employee'].search([('id', 'in', data['employee_ids'])], order='name asc')
            master_list = []
            for emp in employees:
                attendance = self.env['hr.attendance'].search([
                            ('employee_id', 'in', [emp.id]),
                            ('check_in', '>=', data['start_date']),
                            ('check_in', '<=', data['end_date'])]
                        )
                attendance_ids = {}
                for attendance_id in attendance:
                    if attendance_id.employee_id not in attendance_ids:
                        attendance_ids[attendance_id.employee_id] = {'hour':0.0, 'salary' :0.0}
                    hour = attendance_ids[attendance_id.employee_id]['hour']
                    attendance_ids[attendance_id.employee_id]['hour'] = hour+attendance_id.total_hour
                    if attendance_id.salary != 0.0:
                        attendance_ids[attendance_id.employee_id]['salary'] = attendance_ids[attendance_id.employee_id]['hour'] * attendance_id.salary
                master_list.append(attendance_ids)
        currency_id = self.env.user.company_id.currency_id

        attendance_id = False
        if data['report_for'] == 'detail':
            attendance_id = attendance_ids
        elif data['report_for'] == 'summary':
            attendance_id = master_list

        return {
            'doc_ids': self.env.user,
            'currency_id': currency_id,
            'doc_model': 'attendance.report.wizard',
            'data': data,
            'docs': attendance_id,
        }