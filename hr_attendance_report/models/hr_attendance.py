# -*- coding: utf-8 -*-

from openerp import models, fields, api
from datetime import datetime

class HrAttendance(models.Model):
    _inherit = "hr.attendance"
    
    @api.model_cr
    def init(self):
        for rec in self.search([]):
            rec.sudo().write({'name_triger': 'Computing Trigger manually by user '})
    
    @api.multi
    @api.depends('check_in', 'check_out')
    def _compute_total_hour(self):
        for rec in self:
            if rec.check_in and rec.check_out:
                d1 = datetime.strptime(rec.check_in, "%Y-%m-%d  %H:%M:%S")
                d2 = datetime.strptime(rec.check_out, "%Y-%m-%d  %H:%M:%S")
                date_tt = d2 - d1
                rec.total_hour = date_tt.total_seconds()/3600.00

#     @api.multi
#     @api.depends('employee_id','check_in', 'check_out', 'name_triger')
#     def _compute_salary(self):
#         for rec in self:
#             contract_id = self.env['hr.contract'].get_contract(rec.employee_id, rec.check_in, rec.check_out)
#             if contract_id:
#                 contract = self.env['hr.contract'].browse(contract_id[0])
#                 rec.salary = contract.salary
#                 rec.salary_display = rec.total_hour * contract.salary

    @api.multi
    @api.depends('employee_id', 'name_triger')
    def _compute_salary(self):
        for rec in self:
            rec.salary = rec.employee_id.salary
            rec.salary_display = rec.total_hour * rec.employee_id.salary

    total_hour = fields.Float(
        compute = '_compute_total_hour',
        string = 'Total Hour',
        store= True,
    )
    salary = fields.Float(
        string = 'Salary',
        compute = '_compute_salary',
        store= True,
    )
    salary_display = fields.Float(
        string = 'Total Salary',
        compute = '_compute_salary',
        store= True,
    )
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
    name_triger = fields.Char(
        string = "name_triger"
    )

