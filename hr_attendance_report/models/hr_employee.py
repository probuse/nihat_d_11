# -*- coding: utf-8 -*-

from openerp import models, fields, api


class HrEmployee(models.Model):
    _inherit = "hr.employee"
    
    salary = fields.Float(
        string = 'Salary/Hour'
    )
