# -*- coding: utf-8 -*-

from datetime import datetime, timedelta, date
from odoo import models, fields, api, tools
from dateutil.relativedelta import relativedelta, MO,SU


class AttendanceReportWizard(models.TransientModel):
    _name = "attendance.report.wizard"
    
    start_dt = datetime.now().replace(hour=0, minute=0, second=0)
    end_dt = start_dt.replace(hour=23, minute=59, second=59)
    monday = (start_dt + relativedelta(weekday=MO(-1))).replace(hour=0, minute=0, second=0)
    sunday = (start_dt + relativedelta(weekday=SU(+1))).replace(hour=23, minute=59, second=59)
    
    start_date = fields.Date(
        string = "Start Date",
        required = True,
        default=monday,
    )
    end_date = fields.Date(
        string = "End Date",
        required = True,
        default=sunday,
    )
    employee_ids = fields.Many2many(
        'hr.employee',
        string = "Employees",
    )
    employee_id = fields.Many2one(
        'hr.employee',
        string = "Employee",
    )
    report_for = fields.Selection(
        selection = [('detail','By Employee'),
                     ('summary','By Selected Employees')
        ],
        default = 'detail',
        string = "Report For",
        required = True
    )
    
    @api.multi
    def check_report(self):
        data = self.read()[0]
        return self.env.ref('hr_attendance_report.report_hr_attendance').report_action(self, data=data, config=False)   # odoo 11
