# -*- coding: utf-8 -*-


{
    'name': 'Hr Attendance Report',
    'version': '1.0',
    'category' : 'Human Resources',
    'license': 'Other proprietary',
    'summary': """This module allow you to print attendance report.""",
    'description': """
This module allow you to print attendance report.
    """,
    'author': 'Nihat Demir',
    'website': 'www.netron.uk',
    'images': [],
    'depends': [
            'hr_attendance',
#             'hr_contract',
    ],
    'data':[
        'wizard/attendance_report_wizard_view.xml',
#         'views/hr_contract_view.xml',
        'views/hr_employee_view.xml',
        'views/hr_attendance_view.xml',
        'report/hr_attendance_report.xml',
    ],
    'installable' : True,
    'application' : False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
